//
//  main.m
//  Movii_SDK_DEMO_APP
//
//  Created by Mac on 16/07/2019.
//  Copyright © 2019 Mabel-tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
