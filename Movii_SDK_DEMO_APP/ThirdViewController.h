//
//  ThirdViewController.h
//  Movii_SDK_DEMO_APP
//
//  Created by Leandro Ovalle on 8/22/19.
//  Copyright © 2019 Mabel-tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SM_SDK/FaceViewController.h>

@interface ThirdViewController : UIViewController <SMDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backGround;

- (IBAction)cedulaButton:(id)sender;
- (IBAction)tarjetaButton:(id)sender;
- (IBAction)extranjeriaButton:(id)sender;
- (IBAction)pasaporteButton:(id)sender;


@end

