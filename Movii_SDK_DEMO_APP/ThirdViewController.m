//
//  ThirdViewController.m
//  Movii_SDK_DEMO_APP
//
//  Created by Leandro Ovalle on 8/22/19.
//  Copyright © 2019 Mabel-tech. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController

- (IBAction)cedulaButton:(id)sender {
    [self callFaceView:@"1"];
    
  }

- (IBAction)pasaporteButton:(id)sender {
    [self callFaceView:@"2"];
}

- (IBAction)extranjeriaButton:(id)sender {
    [self callFaceView:@"4"];
}


- (IBAction)tarjetaButton:(id)sender {
    [self callFaceView:@"5"];
}


- (void)viewDidAppear:(BOOL)animated {
   [super viewDidAppear:animated];
}


- (void)callFaceView:(NSString *)docType {

    FaceViewController *vc = [[FaceViewController alloc] initWithDelegate:self andDocType:docType];
    [self presentViewController:vc animated:YES completion:nil];
}


- (void)completedWithResult:(Boolean)result andResponse:(TransactionResponse *)response {
    NSLog(@"result: %@ transactionId: %@", result? @"Sucess":@"Failure", response!= nil ? response.TransactionId:@"");
   [self dismissViewControllerAnimated:YES completion:nil];
}


@end

