//
//  FirtsViewController.m
//  Leo
//
//  Created by Leandro Ovalle on 8/21/19.
//  Copyright © 2019 Leandro Ovalle. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()
@property (strong, nonatomic) NSString *timer;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    float seconds = 2.0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self pasarPantalla];      // Se ejecuta el método
});


}

-(void) pasarPantalla{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    UIViewController *cambiarViewController = [storyboard instantiateViewControllerWithIdentifier:@"Second"];
    [self presentModalViewController:cambiarViewController animated:YES];
    
}


@end
