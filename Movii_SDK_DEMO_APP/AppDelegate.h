//
//  AppDelegate.h
//  Movii_SDK_DEMO_APP
//
//  Created by Mac on 16/07/2019.
//  Copyright © 2019 Mabel-tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

